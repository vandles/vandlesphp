-- 微信用户信息
DROP TABLE IF EXISTS `z_user_info`;
CREATE TABLE `z_user_info`
(
    `id`        bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
    `openid`    varchar(50)         NULL DEFAULT '' COMMENT 'openid',
    `nickname`  varchar(50)         NULL DEFAULT '' COMMENT '用户昵称',
    `avatar`    varchar(255)        NULL DEFAULT '' COMMENT '头像',
    `phone`     varchar(20)         NULL DEFAULT '' COMMENT '用户电话',

    `pid`       bigint unsigned     default 0 comment '上级管理员',
    `role`      tinyint(1) unsigned    default 0 comment '角色，目前只有0否，1是',

    `remark`    varchar(512)        NULL DEFAULT '' COMMENT '描述',
    `sort`      bigint(20) UNSIGNED NULL DEFAULT 0 COMMENT '排序权重',
    `status`    tinyint(1) UNSIGNED NULL DEFAULT 1 COMMENT '状态(0.无效,1.有效)',
    `deleted`   tinyint(1) UNSIGNED NULL DEFAULT 0 COMMENT '删除状态(1已删除,0未删除)',
    `create_at` timestamp            NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
    PRIMARY KEY (`id`) USING BTREE,
    INDEX `idx_ai_user_info4` (`openid`) USING BTREE,
    INDEX `idx_ai_user_info5` (`phone`) USING BTREE,

    INDEX `idx_ai_user_info1` (`sort`) USING BTREE,
    INDEX `idx_ai_user_info2` (`status`) USING BTREE,
    INDEX `idx_ai_user_info3` (`deleted`) USING BTREE
) ENGINE = InnoDB
  CHARACTER SET = utf8mb4
  COLLATE = utf8mb4_unicode_ci COMMENT = '微信用户信息'
  ROW_FORMAT = COMPACT;