<?php
namespace vandles;

class Result extends \stdClass {

    static private $instance;
    public $code = 0;
    public $info = '';
    public $data = [];

    public function __construct($code=1, $info='', $data=[]){
        $this->code = $code;
        $this->info = $info;
        $this->data = $data;
    }


    public static function success($info='成功', $data=[]){

        return self::instance(1, $info, $data);
    }
    public static function error($info='失败', $data=[]){
        return self::instance(0, $info, $data);
    }

    public static function state($code=-1, $info='状态', $data=[]) {
        return self::instance($code, $info, $data);
    }

    public static function instance($code=1, $info='成功', $data=[]) {
        if(!self::$instance instanceof self){
            self::$instance = new self($code, $info, $data);
        }

        self::$instance->code = $code;
        self::$instance->info = $info;
        self::$instance->data = $data;

        return self::$instance;
    }
    private function __clone(){}
}