<?php
/**
 *
 * Author: vandles
 * Date: 2021/9/10 15:16
 * Email: <vandles@qq.com>
 **/

namespace vandles\service;

use think\admin\Service;


class Tools extends Service {

    public static function parseUploadFileName($bg) {
        $path = explode('upload', $bg);
        $shortName = "/upload" . $path[1];
        return [
            'shortName' => $shortName,
            'fullName'  => app()->getRootPath() . 'public' . $shortName,
            'url'       => $bg,
        ];
    }

    /**
     * 获取指定长度的随机字母数字组合的字符串
     *
     * @param  int $length
     * @param  int $type
     * @param  string $addChars
     * @return string
     */
    public static function random(int $length = 6, int $type = 0, string $addChars = ''): string
    {
        $str = '';
        switch ($type) {
            case 0:
                $chars = 'ABCDEFGHIJKMNPQRSTUVWXYZabcdefghijkmnpqrstuvwxyz23456789' . $addChars;
                break;
            case 1:
                $chars = str_repeat('0123456789', 3);
                break;
            case 2:
                $chars = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ' . $addChars;
                break;
            case 3:
                $chars = 'abcdefghijklmnopqrstuvwxyz' . $addChars;
                break;
            case 4:
                $chars = "们以我到他会作时要动国产的一是工就年阶义发成部民可出能方进在了不和有大这主中人上为来分生对于学下级地个用同行面说种过命度革而多子后自社加小机也经力线本电高量长党得实家定深法表着水理化争现所二起政三好十战无农使性前等反体合斗路图把结第里正新开论之物从当两些还天资事队批点育重其思与间内去因件日利相由压员气业代全组数果期导平各基或月毛然如应形想制心样干都向变关问比展那它最及外没看治提五解系林者米群头意只明四道马认次文通但条较克又公孔领军流入接席位情运器并飞原油放立题质指建区验活众很教决特此常石强极土少已根共直团统式转别造切九你取西持总料连任志观调七么山程百报更见必真保热委手改管处己将修支识病象几先老光专什六型具示复安带每东增则完风回南广劳轮科北打积车计给节做务被整联步类集号列温装即毫知轴研单色坚据速防史拉世设达尔场织历花受求传口断况采精金界品判参层止边清至万确究书" . $addChars;
                break;
            case 5:
            default:
                $chars = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz' . $addChars;
                break;
        }
        if ($length > 10) {
            $chars = $type == 1 ? str_repeat($chars, $length) : str_repeat($chars, 5);
        }
        if ($type != 4) {
            $chars = str_shuffle($chars);
            $str = substr($chars, 0, $length);
        } else {
            for ($i = 0; $i < $length; $i++) {
                $str .= mb_substr($chars, floor(mt_rand(0, mb_strlen($chars, 'utf-8') - 1)), 1);
            }
        }
        return $str;
    }

    public static function ago1($time = NULL){
        $text = '';
        $time = $time === NULL || $time > time() ? time() : intval($time);
        $t = time() - $time; //时间差 （秒）
        $y = date('Y', $time)-date('Y', time());//是否跨年
        switch($t){
            case $t == 0:
            $text = '刚刚';
            break;
            case $t < 60:
            $text = $t . '秒前'; // 一分钟内
            break;
            case $t < 60 * 60:
            $text = floor($t / 60) . '分钟前'; //一小时内
            break;
            case $t < 60 * 60 * 24:
            $text = floor($t / (60 * 60)) . '小时前'; // 一天内
            break;
            case $t < 60 * 60 * 24 * 3:
            $text = floor($time/(60*60*24)) ==1 ?'昨天 ' . date('H:i', $time) : '前天 ' . date('H:i', $time) ; //昨天和前天
            break;
            case $t < 60 * 60 * 24 * 30:
            $text = date('m月d日 H:i', $time); //一个月内
            break;
            case $t < 60 * 60 * 24 * 365&&$y==0:
            $text = date('m月d日', $time); //一年内
            break;
            default:
            $text = date('Y年m月d日', $time); //一年以前
            break;
        }
        return $text;
    }
    /**
     * 获得几天前，几小时前，几月前
     * @param int $time 时间戳
     * @param array $unit 时间单位
     * @return bool|string
     */
    public static function ago($time, $unit = null) {
        $t = time();
        $time = intval($time);
        $unit = is_null($unit) ? array("年", "月", "星期", "天", "小时", "分钟", "秒") : $unit;
        switch (true) {
            case $time < ($t - 31536000) :
                return floor(($t - $time) / 31536000) . $unit[0] . '前';
            case $time < ($t - 2592000) :
                return floor(($t - $time) / 2592000) . $unit[1] . '前';
            case $time < ($t - 604800) :
                return floor(($t - $time) / 604800) . $unit[2] . '前';
            case $time < ($t - 86400) :
                return floor(($t - $time) / 86400) . $unit[3] . '前';
            case $time < ($t - 3600) :
                return floor(($t - $time) / 3600) . $unit[4] . '前';
            case $time < ($t - 60) :
                return floor(($t - $time) / 60) . $unit[5] . '前';
            default :
                return floor($t - $time) . $unit[6] . '前';
        }
    }

    /**
     *
     * 字符串分隔，回车 
     * 
     */
    public static function split_enter($str){
        return preg_split("/\r\n/",$str);
    }
	
	/*
	 * 生成唯一标志
	 * 标准的UUID格式为：xxxxxxxx-xxxx-xxxx-xxxxxx-xxxxxxxxxx(8-4-4-4-12)
	 */
	function  uuid(){  
		$chars = md5(uniqid(mt_rand(), true));  
		$uuid = substr ( $chars, 0, 8 ) . '-'
				. substr ( $chars, 8, 4 ) . '-' 
				. substr ( $chars, 12, 4 ) . '-'
				. substr ( $chars, 16, 4 ) . '-'
				. substr ( $chars, 20, 12 );  
		return $uuid ;  
	}
	// 安全编码
	function urlsafe_b64encode($string) {
		$data = base64_encode($string);
		$data = str_replace(array('+','/','='),array('-','_',''),$data);
		return $data;
	}
	// 安全解码
	function urlsafe_b64decode($string) {
		$data = str_replace(array('-','_'),array('+','/'),$string);
		$mod4 = strlen($data) % 4;
		if ($mod4) {
			$data .= substr('====', $mod4);
		}
		return base64_decode($data);
	}
	function d(...$args){
		dump(...$args);
	}
	function dd(...$args){
		halt(...$args);
	}
	function domain($port=false){
		return request()->domain($port);
	}
	function now(){
		return date('Y-m-d H:i:s');
	}
	function today(){
		return date('Y-m-d');
	}
	// 字符串长度超出length, 用省略号代替
	function elips($text, $length) {
		if (mb_strlen($text, 'utf8') > $length) {
			return mb_substr($text, 0, $length, 'utf8') . '...';
		} else {
			return $text;
		}
	}
    /**
     * 秒数转时间
     * @param int $delta 秒数
     * @return string
     */
    function sec2str($delta){
        $y = intval($delta / 31536000);
        $delta = $delta - 31536000 * $y;
        $m = intval($delta / 2592000);
        $delta = $delta - 2592000 * $m;
        $d = intval($delta / 86400);
        $delta = $delta - 86400 * $d;
        $h = intval($delta / 3600);
        $delta = $delta - 3600 * $h;
        $i =intval($delta / 60);
        $s = $delta % 60;

        if($s < 10) $s = '0'.$s;
        $str = " $s 秒";

        if($i > 0){
            $i = $i < 10 ?  ' '.$i : $i;
            $str = " $i 分" . $str;
        }
        if($h > 0){
            $h = $h < 10 ?  ' '.$h : $h;
            $str = " $h 小时" . $str;
        }
        if($d > 0){
            $d = $d < 10 ?  ' '.$d : $d;
            $str = " $d 天" . $str;
        }
        if($m > 0){
            $m = $m < 10 ?  ' '.$m : $m;
            $str = " $m 月" . $str;
        }
        if($y > 0){
            $y = $y < 10 ?  ' '.$y : $y;
            $str = "$y 年" . $str;
        }

        return $str;
    }
}