<?php


namespace vandles\richpos\service;


use vandles\richpos\model\BaseModel;
use vandles\richpos\model\BvendorModel;
use vandles\richpos\model\EitemsaleModel;
use vandles\richpos\model\PmaindaybookModel;

class BvendorService extends BaseService {

    public static function vendorLogin($vcode, $pwd) {
        $result = ['code'=>0, 'data'=>[], 'info'=>''];


        $vendor = self::getVendorByVcode($vcode);
        if($vendor){
            if($vendor['password'] != $pwd)
                $result['info'] = '密码错误';
            else{
                $result['code'] = 1;
                $result['data'] = compact('vendor');
                $result['info'] = '用户合法';
            }
        }else
            $result['info'] = '厂商不存在';

        return $result;
    }

    public static function getVendorByVcode($vcode) {
        $vendor = BvendorModel::instance()->where(['vcode'=>$vcode])->find();
        return $vendor;
    }

    // 得到厂商销售
    public static function getSalesData($vcode, $range=null, $shopid=0) {
        $today = today();
        if(!$range) $range = [$today, $today];
        $start = $range[0];
        $end   = $range[1];
        $timeStart = strtotime($range[0]);
        $timeEnd = strtotime($range[1]);
        $time1 = strtotime($today . ' 00:00:00');
        $time2 = strtotime($today . ' 23:59:59');
        // 分3种情况：1. 不包含今天，2. 只有今天，3. 包含今天
        if($timeEnd < $time1 || $timeStart > $time2){

            $list = EitemsaleModel::instance($shopid)->getSalesNoTodayByVcode($vcode, $range);
        }elseif($start == $end && $start == $today){

            $list = PmaindaybookModel::instance($shopid)->getSalesTodayByVcode($vcode);
            if(!$list || !$list[0]['amount']) $list = [];
        }else{

            // 今天的
            $list0 = PmaindaybookModel::instance($shopid)->getSalesTodayByVcode($vcode);

            // 今天（不包含）之前的
            $end = date('Y-m-d', ($time1 - 1));
            $list1 = EitemsaleModel::instance($shopid)->getSalesNoTodayByVcode($vcode, [$start,$end]);

            // 今天（不包含）之后的
            $start = date('Y-m-d', ($time2 + 1));
            $end   = $range[1];
            $list2 = EitemsaleModel::instance($shopid)->getSalesNoTodayByVcode($vcode, [$start,$end]);

            $list = array_merge($list1, $list0, $list2);
        }
        $total = 0;
        foreach($list as &$vo) {
            $vo['amount'] = BaseModel::instance()->str2float($vo['amount']);
            $total += $vo['amount'];

        }
        if($total > 0) $total = sprintf("%01.2f", $total);
        return compact('list', 'total');
    }

    public static function getVendorAll($limit=20) {
        $list = BvendorModel::instance()->limit($limit)->select();
        return $list;
    }



}