<?php


namespace vandles\richpos\service;


use vandles\richpos\model\SshopsettingModel;

class SshopService extends BaseService {

// 得到全部门店列表
    public static function getShopList($field=null) {
        $model = SshopsettingModel::instance();
        if($field) $model = $model->field($field);
        $list = $model->select();

        return $list;
    }

    // 得到分店列表
    public static function getBranchShopList($field=null) {
        $model = SshopsettingModel::instance();
        if($field) $model = $model->field($field);
        $list = $model->where(['shoptype'=>'1'])->select();

        return $list;
    }
}