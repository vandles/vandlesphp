<?php


namespace vandles\richpos\model;


class BvendorModel extends BaseModel {

    public $table = 'bvendor';
    public $pk = 'vcode';
    public $columns = ['vcode','phone','password'];
    public $columnscn = ['address','vendorname'];

}