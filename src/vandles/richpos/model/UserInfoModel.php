<?php


namespace vandles\richpos\model;


use vandles\richpos\db\Db;
use think\Exception;
use think\facade\Request;

class UserInfoModel extends BaseModel {
    public $table = 'user_info';
    public $pk = 'id';
    public $columns = ['password','create_at'];
//    public $columnsfloat = [];
    public $columnscn = ['username'];

}