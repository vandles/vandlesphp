<?php


namespace vandles\richpos\model;


class SshopsettingModel extends BaseModel {
    public $table = 'sshopsetting';
    public $pk = 'shopid';
    public $columns = ['shopid','shoptype','state','longitude','latitude','isonline','firstdate','lastdate'];
    public $columnscn = ['shopname'];

    public function __construct($shopid=0, $single=true){
        parent::__construct($shopid, $single);

        $defaultField = ['shoptype' => 1, 'state'=>0, 'isonline'=>1];

        $this->defaultField = array_merge($defaultField, $this->defaultField);
    }


}