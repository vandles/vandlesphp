<?php


namespace vandles\richpos\model;


class PmaindaybookModel extends BaseModel {
    public $table = 'pmaindaybook';
    public $pk = 'saleno';
    public $columns = ['saleno','saleflag','saledate','posno','receiverno','memberno','addimemberno','shopid','ifwxvoucher','voucherno'];
    public $columnsfloat = ['shouldcharge','actualcharge','depreciate','membercard','cash','alipay','weixin','adduppoints','relatevoucher'];
//    public $columnscn = ['shopname'];

    // 今天 的销售
    public function getSalesTodayByVcode($vcode) {
        $today = today();
        $sql = "SELECT '$today' as saledate,  sum(b.actualcharge) amount
           FROM pmaindaybook a,
            psubdaybook b 
          WHERE a.saledate = :today and
            a.saleflag in ('0','9') and
            a.saleno = b.saleno and
            b.vcode =:vcode";

        $map = [
            ':today' => $today,
            ':vcode' => $vcode,
        ];
        return $this->executeList($sql, $map);
    }

    // 得到今天的流水
    public function getSaleFlowTodayByVcode($vcode, $limit=100) {
        $today = today('Y-m-d');

        $sql = "select  top $limit b.saletime,c.itemname,b.salenumber,b.actualcharge
            from rich.pmaindaybook a,
            rich.psubdaybook b,
            bitem c 
            where a.saledate = :today and
            a.saleflag in ('0', '9') and
            a.saleno = b.saleno and
            b.vcode = :vcode and
            b.icode = c.icode
            order by b.saletime desc";


        $map = [
            ':today' => $today,
            ':vcode' => $vcode,
        ];

        $list = $this->executeList($sql, $map, ['itemname'],['salenumber','actualcharge']);

        return $list;
    }

}