<?php


namespace vandles\richpos\model;


class PsubdaybookModel extends BaseModel {
    public $table = 'psubdaybook';
    public $pk = 'saleno';
    public $columns = ['saleno','saletime','serialno','icode','taxrate','gcode','vcode'];
    public $columnsfloat = ['purprice','cusprice','salenumber','shouldcharge','actualcharge','depreciate'];
//    public $columnscn = ['shopname'];



}