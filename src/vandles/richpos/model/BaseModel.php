<?php


namespace vandles\richpos\model;


use think\facade\Request;
use think\route\AliasRule;
use vandles\richpos\db\Db;

class BaseModel {
    public $shopid = 0;
    public $db = '';
    public $table = '';
    public $pk = 'id';
    public $columns = []; // 普通字段
    public $columnscn = []; // 中文字段
    public $columnsfloat = []; // 浮点字段
    public $defaultField = []; // 没用，但richpos库不能为空的字段
    public $insertDefault = true; // 插入默认的firstdate等4个字段，因为有的表没有这4个字段，新增的时候要去掉。

    public $isForce = false; // 是否强制操作，多用于删除

    protected $where = [];

    public function __construct($shopid=0, $single=true){
        $this->shopid = $shopid;
        $this->shopRequired();

        $this->db = Db::instance($shopid, $single);

//        $now = date('Y-m-d H:i:s');
//        $this->defaultField = [
//            'firstdate' => $now, 'lastdate'=>$now,
//            'firstman' => '9999', 'lastman' => '9999',
//        ];
        $this->__initialize();
    }

    public function __initialize(){

    }

    public static function instance($shopid=0, $single=true){
        return new static($shopid, $single);
    }


    public function find(){
        $list = $this->select();
        if(isset($list[0])){
            return $list[0];
        }else{
            return [];
        }
    }

    public function select(){
        if(empty($this->columns) && empty($this->columnscn) && empty($this->columnsfloat)) $this->columns = '*';
        else $this->columns = array_merge($this->columns, $this->columnscn, $this->columnsfloat);

        $this->where = $this->filterWhere($this->where);
//        dd($this->where);
        $list = $this->db->select($this->table, $this->columns, $this->where);

        return $this->filterRead($list);
    }

    public function create($data=null) {
        if(isset($data['_token_'])) unset($data['_token_']);

        foreach ($data as $field => $value) {
            if(in_array($field, $this->columnscn)){
                $data[$field] = $this->write2gbk($value);
            }else if(in_array($field, $this->columnsfloat)){
                $data[$field] = $this->str2float($value);
            }else{
                $data[$field] = $value;
            }
        }
        $this->insertDefault && $data = array_merge($data, $this->defaultField);
        $result = $this->db->insert($this->table, $data);
        return $result->rowCount();
    }

    public function update($data, $where){
        if(isset($data['_token_'])) unset($data['_token_']);
        foreach ($data as $field => $value) {
            if(in_array($field, $this->columnscn)){
                $data[$field] = $this->write2gbk($value);
            }else if(in_array($field, $this->columnsfloat)){
                $data[$field] = $this->str2float($value);
            }else{
                $data[$field] = $value;
            }
        }
        $result = $this->db->update($this->table, $data, $where);
        return $result->rowCount();
    }

    // 用于强制删除
    public function force($isForce = false) {
        $this->isForce = $isForce;
        return $this;
    }

    public function delete($where){
        !$this->isForce && halt("出于数据安全考虑，禁用【删除】功能（强制模式除外）");
        $result = $this->db->delete($this->table, $where);
        return $result->rowCount();
    }

    public function where($where=[]) {
        $this->where = array_merge($this->where, $where);
        return $this;
    }
    public function limit($limit=10) {
        $where['LIMIT'] = $limit;
        $this->where = array_merge($this->where, $where);
        return $this;
    }

    /**
     * @param $order
     * @return $this
     *
     * $order = "id desc"
     * $order = "id"
     *
     */
    public function order($order){
        $order = explode(' ', $order);
        $len = count($order);
        if($len == 2) list($field, $sort) = $order;
        else if($len == 1) list($field, $sort) = [$order[0], 'ASC'];
        else dd('order格式错误！');

        $where = [
            'ORDER' => [$field => strtoupper($sort)]
        ];
        $this->where = array_merge($this->where, $where);
        return $this;
    }

    public function count() {
        return $this->db->count($this->table, $this->where);
    }
    public function sum($column) {
        return $this->db->sum($this->table, $column, $this->where);
    }

//    public function query(){
//
//    }

    // 设置显示字段
    public function field($fields){
        $arr = explode(',', $fields);
        $columns = [];
        $columnscn = [];
        $columnsfloat = [];
        foreach ($arr as $field) {
            if(in_array($field, $this->columnscn)){
                $columnscn[] = $field;
            }else if(in_array($field, $this->columnsfloat)){
                $columnsfloat[] = $field;
            }else{
                $columns[] = $field;
            }
        }
        $this->columns = $columns;
        $this->columnscn = $columnscn;
        $this->columnsfloat = $columnsfloat;
        return $this;
    }

    // 条件过滤器
    protected function filterWhere($where){
        foreach ($where as $k => $v){
//            if($k == 'ORDER') continue;
            if(is_array($v)){
                $where[$k] = $this->filterWhere($v);
            }else if(is_string($v)){
                list($field) = explode('[', $k);
                if(in_array($field, $this->columnscn)){
                    $where[$k] = $this->write2gbk($v);
                }
            }
        }
        return $where;
    }

    public function like($fields){
        $get = Request::get();
        $fields = explode(',', $fields);
        $where = [];
        foreach ($fields as $field){
            if(isset($get[$field]) && $get[$field] != ''){
                $where[$field.'[~]'] = $get[$field];
            }
        }
        $this->where = array_merge($this->where, $where);
        return $this;
    }
    public function equal($fields){
        $get = Request::get();
        $fields = explode(',', $fields);
        $where = [];
        foreach ($fields as $field) {
            if (isset($get[$field]) && $get[$field] != '') {
                $where[$field] = $get[$field];
            }
        }
        $this->where = array_merge($this->where, $where);
        return $this;
    }

    // 读取过滤器
    public function filterRead($list, $cb=null){
        $data = [];
//        dd($list);
        foreach($list as $vo) {
            $item = [];
            foreach ($vo as $field => $value){
//                if(is_numeric($field)) continue;
                $value = trim($value);
                if(in_array($field, $this->columnscn)){
                    $item[$field] = $this->read2utf8($value);
                }else if(in_array($field, $this->columnsfloat)){
                    $item[$field] = $this->str2float($value);
                }else if(in_array($field, $this->columns)){
                    $item[$field] = $value;
                }

            }
            if ($cb) {
                $item = call_user_func($cb,$item);
            }
            $method = '_order_filter';
            if (method_exists($this, $method)){
                $item = $this->$method($item);
            }
            $data[] = $item;
        }
        return $data;
    }

    protected function quote($var, $l=''){
        $var = empty($l) ? "'{$var}'" : "'%{$var}%'";
        return $var;
    }

    // 字符转码 - 从sqlserver读取时用
    private function read2utf8($str){
        return iconv("GB18030//IGNORE", "UTF-8", $str);
    }
    // 字符反转码 - 写入sqlserver时用
    private function write2gbk($str){
        return iconv("UTF-8", "GB18030//IGNORE", $str);
    }

    // 转浮点
    public function str2float($str) {
        return floatval(sprintf("%.2f", $str));
    }

    // 解析时间字符串，用于查询
    public function parseQueryDate($range){
        list($start, $end) = explode(' - ', $range);
        $start = $start . ' 00:00:00';
        $end = date('Y-m-d H:i:s', strtotime($end . ' 23:59:59') + 60);
        return [$start, $end];
    }

    private function shopRequired(){
        $className = get_class($this);
        $className = substr($className, strrpos($className,'\\')+1);
        if(($className == 'GoodsModel' || $className == 'BgroupModel') && !$this->shopid){
            halt('商品相关的Model缺少门店信息！');
        }
    }

    // sql语句查询用的方法 查询数量专用方法，用于分页
    protected function queryTotal($sql_count){
        $res = $this->db->query($sql_count)->fetchAll();
        $total = 0;
//        dd($res);
        foreach($res as $vo){
            $total += $vo[0];
        }
        return $total;
    }

    // sql语句查询用的方法
    protected function parseWhere($where){
        if(empty($where)) return '';
        $conStr = '';
        $where = $this->filterWhere($where);
        foreach($where as $type=>$vo){
            if($type == '~'){
                foreach($where['~'] as $field=>$value){
                    $conStr .= " and $field like " . $this->quote($value);
                }
            }else if($type == '='){

                foreach($where['='] as $field=>$value){
                    $conStr .= " and $field = " . $value;
                }
            }
        }
        return $conStr;
    }

    /**
     * 执行sql语句时用的方法
     * @param $sql
     * @param $map
     * @param array $colCN
     * @param array $colFloat
     * @return array
     */
    public function executeList($sql, $map, $colCN=[], $colFloat=[]) {
        $list = $this->db->query($sql, $map)->fetchAll(2);

        foreach ($list as &$vo){
            foreach ($vo as $field => $value){
                if(in_array($field, $colCN)){
                    $vo[$field] = $this->read2utf8($value);
                }else if(in_array($field, $colFloat)){
                    $vo[$field] = $this->str2float($value);
                }
            }
        }

        return $list;
    }

}