<?php


namespace vandles\richpos\model;


class EitemsaleModel extends BaseModel {

    public $table = 'eitemsale';
    public $pk = '';
    public $columns = ['shopid','saleyear','salemonth','saletime','icode','dcode','gcode','mcode','scode','vcode'];
    public $columnsfloat = ['salenumber','shouldcharge','actualcharge'];
    public $columnscn = ['shopname'];

    // 非今天 的销售
    public function getSalesNoTodayByVcode($vcode, $range) {
        $sql = "  SELECT CONVERT(varchar(100), EITEMSALE.SALETIME, 23) saledate,
            sum(EITEMSALE.ACTUALCHARGE) amount
             FROM EITEMSALE
           WHERE  ( EITEMSALE.VCODE = :vcode ) and
              ( EITEMSALE.SALETIME between :start and :end )
        group by EITEMSALE.SALETIME
        order by EITEMSALE.SALETIME";

        $map = [
            ':start' => $range[0],
            ':end' => $range[1],
            ':vcode' => $vcode,
        ];
        $list = $this->db->query($sql, $map)->fetchAll(2);
        return $list;
    }

}