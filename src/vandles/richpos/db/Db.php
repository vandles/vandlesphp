<?php
namespace vandles\richpos\db;

require_once 'Medoo.php';


use Medoo\Medoo;

class Db extends Medoo{

    static private $instance;
    static private $instance_shop;

    //防止直接创建对象
    function __construct($config){
        parent::__construct($config);
    }
    //防止克隆对象
    private function __clone(){
    }
    public static function instance($shopid=0, $single=true){
//        $dbConfig = self::dbConfig();
//        dd($dbConfig);
        if(empty($shopid)){
            if($single){
                if (!self::$instance instanceof self) {
                    $medoo = self::dbConfig('medoo');
                    self::$instance = new self($medoo);
                }
                return self::$instance;
            }else{
                $medoo = self::dbConfig('medoo');
                return new self($medoo);
            }
        }else{
            if($single){
                if (!self::$instance_shop instanceof self) {
                    $medoo = self::dbConfig($shopid);
                    self::$instance_shop = new self($medoo);
                }
                return self::$instance_shop;
            }else{
                $medoo =self::dbConfig($shopid);
                return new self($medoo);
            }
        }
    }

    public static function close(){
        self::$instance = null;
        self::$instance_shop = null;
    }

    private static function dbConfig($shopid='medoo'){
        $config = require_once 'config.php';
        $richposConfig = [
            'database_type' => 'mssql',
            'server'        => $config['zb']['ip'],
            'username'      => $config['username'],
            'password'      => $config['password'],
            'pdo'           => 'finial'

//            'sql_version'   => 'sql2000',
//            'port'          => '1433',
//            'charset'       => 'Chinese_PRC_CI_AS',
        ];

        // 总总
        $dbConfig['db_medoo'] = array_merge($richposConfig, ['database_name' => $config['zb']['dbname']]);

        // 各分店
        foreach($config['subs'] as $sub){
            $richposConfig['server'] = $sub['ip'];
            $dbConfig["db_$sub[shopid]"] = array_merge($richposConfig, ['database_name' => "$sub[dbname]"]);
        }


        $shopConfig = $dbConfig['db_'.$shopid];
        if(empty($shopConfig)){
            echo "shopid=$shopid 数据库配置不存在。";die;
        }
        return $dbConfig['db_'.$shopid];
    }
}