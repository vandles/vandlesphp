<?php
return [
    'username' => 'rich',
    'password' => 'ultratradestudio',

    // 总部
    'zb' => [
        'ip'     => '127.0.0.1',
//        'shopid' => '999999',
        'dbname'     => 'zbrichpos_t'
    ],

    // 各分店
    'subs' => [

        // 方恒店 richpos01_t
        '000001' => [
            'ip'     => '127.0.0.1',
            'shopid' => '000001',
            'dbname' => 'richpos01_t',
        ],

        // 悠唐店 richpos02_t
        '000002' => [
            'ip'     => '127.0.0.1',
            'shopid' => '000002',
            'dbname' => 'richpos02_t',
        ]
    ],


];